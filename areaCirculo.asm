.data
	PI: .float 3.14159
	raio: .float 0.0
	msg1: .asciiz  "Digite o valor do raio do circulo, e ENTER\n" 
	msg2_1: .asciiz "Raio digitado e' "
	msg2_2: .asciiz " \n"
	msg3_1: .asciiz "Area do circulo com raio "
	msg3_2: .asciiz " e' "
	msg3_3: .asciiz "\n"
	aux1: .float 0.50
	aux2: .float 100
	aux3: .word 1000000000000
	aux: .word 0

.text
.globl main

main:	
	#mepamento de vars
	lwc1 $f1, aux1
	lwc1 $f2, aux2
	#lwc1 $f3, aux3
	la   $s3, aux3
	lw   $s3, 0($s3)
	lwc1 $f6, PI
	
	#print primeira mensagem (Digite o valor do raio do circulo, e ENTER\n)
	la $a0, msg1
	li $v0, 4
	syscall
	
	#scanf do raio
	li $v0, 6
	syscall
	mov.s $f5, $f0	
	
	# 2 casas decimais arredondado [(>= 0.5) = 1; (< 0.50) => 0]
	mul.s $f10, $f5, $f2
	add.s $f10, $f10, $f1
	cvt.w.s $f10, $f10
	cvt.s.w $f10, $f10
	div.s $f10, $f10, $f2	
	
	#print segunda mensagem (Raio digitado e' %12.2f)
	la $a0, msg2_1
	li $v0, 4
	syscall
	
	#print raio
	mov.s $f12, $f10
	li $v0, 2
	syscall
	
	#print segunda mensagem (\n)
	la $a0, msg2_2
	li $v0, 4
	syscall
	
	#print terceira mensagem (Area do circulo com raio)
	la $a0, msg3_1
	li $v0, 4
	syscall
	
	#print raio
	mov.s $f12, $f5
	li $v0, 2
	syscall
	
	#print terceira mensagem ( e' )
	la $a0, msg3_2
	li $v0, 4
	syscall
	
	#prepara paramentro da func,ao
	swc1 $f5, -88($fp)
	lw $a0, -88($fp)
	
	#chama a func,ao
	sub $sp, $sp, 4
	sw $ra, 0($sp)
	jal AreaCirc
	
	#print areaCirc
	lwc1 $f12, 0($sp)
	addiu $sp, $sp, 4
	li $v0, 2
	syscall
	
	#print terceira mensagem (\n)
	la $a0, msg3_3
	li $v0, 4
	syscall
	
	#fim
	li $v0, 10
	syscall
	
	AreaCirc:
	sub $sp, $sp, 12
	swc1 $f1, 0($sp)
	sw $t0, 4($sp)
	
	add $t0, $a0, $zero
	
	sw $t0, -88($fp)
	lwc1 $f1, -88($fp)
		
	mul.s $f1, $f1, $f1
	mul.s $f1, $f1, $f6
	
	swc1 $f1, 8($sp)
	lwc1 $f1, 0($sp)
	lw $t0, 4($sp)
	
	addiu $sp, $sp, 8
	jr $ra
	
