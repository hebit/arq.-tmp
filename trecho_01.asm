.data
  i: .word 0
  max: .word 20
  x: .word 0
  y: .word 0
  vetor:.word 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

.text
.globl main

main:

la $s1, i
lw $s1, 0($s1)
la $s2, max
lw $s2, 0($s2)
la $s3, x
lw $s3, 0($s3)
la $s4, y
lw $s4, 0($s4)


la $s5, vetor
la $s6, x
la $s7, y

for:
#condicao do for:
bge $s1, $s2, forEnd

#le o valor de x
li $v0, 5
syscall

#carrega o valor da entrada a para X:
add $s3, $v0, $zero
sw  $s3, 0($s6)

#calcula o deslocamento (i) para o vetor:
add $t0, $s1, $zero
add $t0, $t0, $t0
add $t0, $t0, $t0

#altera o vetor para a posiçao i e guarda o valor de x na posicao:
add $s5, $s5, $t0
sw  $s3, 0($s5)

#calcula e armazena em y:
li  $t1, 0
lw  $t1, 0($s5) # = vetor[i]
add $t1, $t1, $s3 # + x
add $t1, $t1, $s1 # + i
sw  $t1, 0($s7)

#li  $v0, 1
#lw $a0, 0($s7)
#syscall

#retorna o vetor para a posiçao inicial
sub $s5, $s5, $t0

#incrementa i
add $s1, $s1, 1

j for

forEnd:

li $v0, 10
syscall
