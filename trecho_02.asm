.data
  x: .word 10
  max: .word 50
  y: .word 0
  z: .word 

.text
.globl main

main:
la $s5, x
lw $s1, 0($s5)
la $s6, y
lw $s2, 0($s6)
la $s7, z
lw $s3, 0($s7)
la $s4, max
lw $s4, 0($s4)

while:
#calcula e armazena z:
add $s3, $s1, $s1
add $s3, $s3, $s2
sw  $s3, 0($s7)

#li $v0, 1
#add $a0, $s3, $zero
#syscall 

#subtrai 1 de y:
sub $s2, $s2, 1
sw  $s2, 0($s6)

#le um novo valor pra x:
li $v0, 5
syscall

#armazena x:
add $s1, $v0, $zero
sw  $s1, 0($s5)

#condicao do do-while:
bge $s1, $s4, whileEnd
j while

whileEnd:

li $v0, 10
syscall